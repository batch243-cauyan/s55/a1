
import { useState, useEffect,useContext } from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
// require('dotenv').config();
// let baseURL = "http://localhost:5000"
let baseURL1 = process.env.REACT_APP_URL
const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    // Allows us to consume the User context object and its properties to use for user validation
    const {user, setUser, token, setToken} = useContext(UserContext);

    useEffect(()=>{
        if (email && password){
            setIsActive(true);
        }else{
            setIsActive(false)
    }
    },[email,password])


    function loginUser(event){
        event.preventDefault();
        console.log(process.env.REACT_APP_URL)
        // alert("Logged In Successfully.")

        // // Storing information in the local storage will make the data persistent even as the page is refreshed unlike with the use of states where information is reset when page is reloaded.
        // localStorage.setItem('email',email);

        // Set the global user state to have properties obtained from the local storage.
        // though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing.


        // Process wherein it will fetch a request to the corresponding API
        // The header information "Content-Type" is used to specify that the information being sent to the backend will be sent in the form of json.
        // The fetch request will communicate with our backend application providing it with a stringified JSON
        // Syntax:
            // fetch('url', {options:method, headers, body})
            // .then(res=> res.json())
            // .then(data => {data process})
            // 
        localStorage.setItem('email',email);
        
        // setUser(localStorage.getItem("email"));
      
        fetch(`${baseURL1}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email, password:password
            })
        })
        .then(res=>{
            return res.json();
        })
        .then(data =>{
            if (data.accessToken!=='empty'){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);
                console.log(email, password);
                console.log(data);
                Swal.fire({
                    title:"Login Successful",
                    icon: "success",
                    text: "Welcome to our website!"
                })
            }else{
                Swal.fire({
                    title:"Authentication Failed",
                    icon: "error",
                    text: "Wrong credentials, please try again!"
                })
                
                setPassword('');
            }
        })

        const retrieveUserDetails = (token)=>{
            fetch(`${baseURL1}/users/profile`,{
                headers:{
                    Authorization:`Bearer ${token}`
            }})
            .then(res=>{
                return res.json()
            })
            .then(data=>{
                console.log(data);
                setUser({id:data._id, isAdmin:data.isAdmin});
                console.log(user);

            })
        }

        // setEmail('');
    }


    return (
        
            (user.id) ? 
            <Navigate to ="/"/>
            :
                <Container>
                        <Row>
                            <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                                <Form className='p-3' onSubmit={loginUser}>
                                    <h1>Login</h1>
                                    <Form.Group className="mb-3" controlId="email">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control type="email" placeholder="Enter email" required value={email} onChange={event =>{
                                            setEmail(event.target.value)
                                        }}/>
                                        
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="password1">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" required value={password} onChange={event =>{
                                            setPassword(event.target.value)
                                        }}/>

                                    </Form.Group>
                                    
                                    <Button className ="btn-lg"variant="success" type="submit" disabled={!isActive} >
                                        Login
                                    </Button>
                                </Form>
                        </Col>
                    </Row>
                </Container>
        
     );
}
 
export default Login;