import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import Banner from "../components/Banner";


const Error = () => {


    return ( 
        <div>
            <Banner/>
            <h1>Page Not Found</h1>
            <p>Go back to the <Link to="/"><Button >homepage</Button></Link>
            </p>
        </div>
     );
}
 
export default Error;