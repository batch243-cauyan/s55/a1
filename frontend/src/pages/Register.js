import { useState, useEffect, useContext} from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


const Register = () => {
    
const {user,setUser} = useContext(UserContext);

const history = useNavigate();
    // Statehooks
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNum, setMobileNum] = useState('');

    useEffect(()=>{
        if (email && password1 && password2 && firstName && lastName && mobileNum && password1===password2){
            setIsActive(true);
        }else{
            setIsActive(false)
    }

    console.log(email);
    console.log(password1);
    console.log(password2);

    },[email,password1,password2, firstName, lastName, mobileNum])

    function registerUser(event){
        event.preventDefault();

        fetch(`${process.env.REACT_APP_URL}/users/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email:email, 
                password:password1, 
                firstName: firstName,
                lastName:lastName,
                mobileNo: mobileNum
            })
        })
        .then((res)=> {
            
   
            // console.log(`This is response`,response);
            // if(!res.ok){
            //     return Swal({
            //         title: "Registration Failed Successfully",
            //         icon: "error",
            //         text: "Something went wrong"
            //     })
            // }
            // else{  }
            return res.json();
            
        })
        .then(data=>{
            console.log(data);
            if (data.error){
                Swal.fire({
                    title:"Duplicate email",
                    icon:'warning',
                    text:data.error
                })
            }
            else{
                localStorage.setItem('email',email);
                setUser(localStorage.getItem('email'));
                setEmail('');
                setPassword1('');
                setPassword2('');
                setFirstName('');
                setLastName('');
                setMobileNum('');
                Swal.fire({
                    title: "Registration Success",
                    icon: "success",
                    text:"Welcome! You are now registered to our website."
                })
                history('/login');
            }

        })
    }

    return (
        
        (user.id)?
        <Navigate to='/'/>
        :
        <Container>
        <Row>
            <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                <Form className='bg-secondary p-3' onSubmit={registerUser}>
                    <Form.Group className="mb-3 " controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={event =>{
                            setFirstName(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3 " controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={event =>{
                            setLastName(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3 " controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" required value={email} onChange={event =>{
                            setEmail(event.target.value)
                        }}/>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password1">
                        <Form.Label>Enter your desired Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" required value={password1} onChange={event =>{
                            setPassword1(event.target.value)
                        }}/>

                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password2">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" required value={password2} onChange={event =>{
                            setPassword2(event.target.value)
                        }}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="mobileNum">
                        <Form.Label>Mobile Number</Form.Label>
                        <Form.Control minLength={11} type="text" placeholder="Mobile Number" required value={mobileNum} onChange={event =>{
                            setMobileNum(event.target.value)
                        }}/>
                    </Form.Group>
                   <div className='text-center'>
                    <Button className ="btn-lg"variant="primary" type="submit" disabled={!isActive} >
                        Register
                    </Button>
                    </div>
                </Form>
        </Col>
        </Row>
        </Container> 
  );
}

 
export default Register;