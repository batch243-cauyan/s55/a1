import {Fragment, useEffect, useState} from 'react'
import { Row,Col, Container} from 'react-bootstrap';
import CourseCard from '../components/CourseCard';
// import courseData from '../data/courses.js';
// let courseData =[];
export default function Courses(){
	
	const [courseData, setCourseData] = useState([])
	// Syntax:
	// localStoraget.getItem("propertyName")
	// localStorage.getItem('email);


	// const local = localStorage.getItem('email');
	// console.log(local);
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URL}/courses/allActiveCourses`)
		.then(res=>
			 res.json()
		)
		.then(data=>{
			setCourseData(data.map(course => {
				return(
						<CourseCard key = {course._id} prop ={course}/>
					)
			}));
		})
		
	},[])


	// const courses = courseData.map(course => {
	// 	return(
	// 			<CourseCard key = {course._id} prop ={course}/>
	// 		)
	// })

	
	return(

		<Row className='courseRow'>
			<Col>
				{courseData}
			</Col>
		</Row>

		)
}