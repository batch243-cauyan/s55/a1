import { useEffect, useState } from "react";
import { Container,Card, Button, Row, Col } from "react-bootstrap";
import { formatter } from "../Currency";
import { useParams, useNavigate} from "react-router-dom";
import Swal from "sweetalert2";

const CourseView = () => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    let {courseId} = useParams();
    console.log('This is the id',courseId);

    const history = useNavigate();

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_URL}/courses/${courseId}`,{})
        .then((res)=>res.json())
        .then(data=>{
            console.log(data);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        });
    },[courseId])

    const enroll = async function(courseId){
        
        // let user = await fetch(`${process.env.REACT_APP_URL}/users/profileDetails/${courseId}`)


        fetch(`${process.env.REACT_APP_URL}/users/enroll/${courseId}`,{
            method: 'POST',
            headers:{
                "Content-Type": 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res=>res.json())
        .then(data=>{
            if(data ===true){
                Swal.fire({
                    title: "Successfully enrolled!",
                    icon: "success",
                    text: "No Turning back :)"
                })
                history('/courses');
            }else{
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Dasurv"
                })
                history('/');
            }
        })
    }   

    return ( 
        <Container>
            <Row>
                <Col lg={{span:6, offset:3}} >
                    <Card className="classDetails">
					    <Card.Body className="text-center">
									<Card.Title>{name}</Card.Title>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									<Card.Text>{formatter.format(price)}</Card.Text>
									<Card.Subtitle>Class Schedule</Card.Subtitle>
									<Card.Text>8 am - 5 pm</Card.Text>
									<Button variant="success" onClick={()=>enroll(courseId)}>Enroll</Button>
						</Card.Body>	
					</Card>
                </Col>
            </Row>
        </Container>
     );
}
 
export default CourseView;