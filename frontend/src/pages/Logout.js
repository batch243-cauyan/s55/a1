import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect } from "react";
import Swal from "sweetalert2";

const Logout = () => {

    const {unSetUser} = useContext(UserContext);

    
    
    useEffect(()=>{
        unSetUser();
    })

    Swal.fire({
        title:"Welcome back to the outside world!",
        icon: "info",
        text: "Babalik ka rin!"
    })
    return ( 
        <Navigate to ="/login"/>
     );
}
 
export default Logout;