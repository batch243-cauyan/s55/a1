import {useEffect, useState, useContext} from 'react';
import {Row, Col, Button, Card} from 'react-bootstrap'
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { formatter } from '../Currency';

export default function CourseCard({prop}){

	//destructuring the courseProp that were passed from the Course.js
	/*const {name, description, price} = courseProp;*/
	

	//Use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
	//States are used to keep track information related to individual components
	//Syntax:
		// const [getter, setter] =useState(initialGetterValue)
	const {_id, name,description, price, slots} =prop;
	const {user} = useContext(UserContext);
	const [enrollees, setEnrollees] = useState(0);
	const [slotsAvailable, setSlotsAvailable] = useState(slots);
	const [isAvailable, setIsAvailable] = useState(true);
	

	// Add a useEffect hook to have "CourseCard" component to perform a certain task after every DOM update
		// Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

	useEffect(()=>{
		if(slotsAvailable===0){
			
			setIsAvailable(false);
		}
	}, [slotsAvailable, enrollees])

	/*setEnrollees(1)*/

	function enroll(){
		console.log(`This is the prop`,prop);
		// My code
		// if(slotsAvailable>0){
		// 	setEnrollees(enrollees+1);
		// 	setSlotsAvailable(slotsAvailable-1);
		// }
		// else{
		// 	// document.getElementById(`btn${id}`).disabled = true;
		// 	alert('No more seats.')
			
		// }
			if(slotsAvailable===1){
				alert("Congrats, you were able to enroll before the cut!")
			}
			setEnrollees(enrollees+1);
			setSlotsAvailable(slotsAvailable-1);
		}
	// since enrollees is declare as a constant variable, directly reassigning the value is not allowed or will cause an error
	/*enrollees = 1;
	console.log(enrollees);*/

	// const formatter = new Intl.NumberFormat('en-US', {
	// 	style: 'currency',
	// 	currency: 'PHP',
	  
	// 	// These options are needed to round to whole numbers if that's what you want.
	// 	//minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	// 	//maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	//   });


	return(
		
		<Row className="cardRow">
			<Col xs = {12} md = {4} className = "offset-md-4 offset-0">
				<Card className='courseCards'>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>

				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>
				         	{description}
				        </Card.Text>

				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>
				       		{formatter.format(price)}
				        </Card.Text>

				        <Card.Subtitle>Enrollees</Card.Subtitle>
				        <Card.Text>
				         	{enrollees}
				        </Card.Text>

				        <Card.Subtitle>Slots Available:</Card.Subtitle>
				        <Card.Text>
				         	{slotsAvailable} slots
				        </Card.Text>
					{
						(user.id)?
						<Button as={Link} to={`/courses/${_id}`} id={`btn${_id}`} variant="primary" onClick = {enroll} disabled ={!isAvailable} >Details</Button>
						:
						<Button id={`btn${_id}`} variant="primary" as={Link} to="/login" disabled ={!isAvailable} >Enroll</Button>
					}
						

						{/* My Code using Id to disable */}
				        {/* <Button id={`btn${id}`} variant="primary" onClick = {enroll} >Enroll</Button> */}
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

	)
}