//import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){
	
	return(
		<Row className = "mt-3 mb-3">
		{/*This is the first card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      
				      <Card.Body>
				        <Card.Title>
				        	<h2>Learn From Home</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit arcu, mollis vitae mi et, blandit dictum ante. Aliquam erat volutpat. Nullam eu arcu non ex convallis mattis. Integer ac molestie metus. Nunc velit erat, hendrerit id elementum eu, accumsan rhoncus tellus. Nunc fringilla enim at nunc auctor, et placerat tellus dapibus. Quisque vitae faucibus orci. Vestibulum vestibulum et massa ut blandit. Duis finibus ornare condimentum. Nullam rutrum lectus mauris, a varius augue consectetur ac. Vivamus a mollis tellus. Nulla aliquet tincidunt commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer at diam lacus. Phasellus pellentesque nisi venenatis sagittis fermentum. Sed auctor at mi quis condimentum.
				        </Card.Text>
				     
				      </Card.Body>
				    </Card>
			</Col>

			{/*This is the second card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      
				      <Card.Body>
				        <Card.Title>
				        	<h2>Study Now, Pay Later</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit arcu, mollis vitae mi et, blandit dictum ante. Aliquam erat volutpat. Nullam eu arcu non ex convallis mattis. Integer ac molestie metus. Nunc velit erat, hendrerit id elementum eu, accumsan rhoncus tellus. Nunc fringilla enim at nunc auctor, et placerat tellus dapibus. Quisque vitae faucibus orci. Vestibulum vestibulum et massa ut blandit. Duis finibus ornare condimentum. Nullam rutrum lectus mauris, a varius augue consectetur ac. Vivamus a mollis tellus. Nulla aliquet tincidunt commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer at diam lacus. Phasellus pellentesque nisi venenatis sagittis fermentum. Sed auctor at mi quis condimentum.
				        </Card.Text>
				     
				      </Card.Body>
				    </Card>
			</Col>
			{/*This is the third card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      
				      <Card.Body>
				        <Card.Title>
				        	<h2>Be part of our community</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin velit arcu, mollis vitae mi et, blandit dictum ante. Aliquam erat volutpat. Nullam eu arcu non ex convallis mattis. Integer ac molestie metus. Nunc velit erat, hendrerit id elementum eu, accumsan rhoncus tellus. Nunc fringilla enim at nunc auctor, et placerat tellus dapibus. Quisque vitae faucibus orci. Vestibulum vestibulum et massa ut blandit. Duis finibus ornare condimentum. Nullam rutrum lectus mauris, a varius augue consectetur ac. Vivamus a mollis tellus. Nulla aliquet tincidunt commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer at diam lacus. Phasellus pellentesque nisi venenatis sagittis fermentum. Sed auctor at mi quis condimentum.
				        </Card.Text>
				     
				      </Card.Body>
				    </Card>
			</Col>
		</Row>
		)
}