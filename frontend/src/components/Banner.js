import Button from "react-bootstrap/Button";
/*Bootstrap Grid system*/
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import { Link } from "react-router-dom";


export default function Banner(){

	return(
		<Row className="text-center">
			<Col>
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Button as={Link} to="/courses" variant = "primary">Enroll now!</Button>
				
			</Col>
		</Row>
		)
}