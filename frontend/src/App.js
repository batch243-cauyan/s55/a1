



// [Pages]
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';


// import {Fragment} from 'react';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import CourseView from './pages/CourseView';
import { Container } from 'react-bootstrap';

function App() {



  // State hook for the user that will be globally accessible using the useContext
  // This will also be used to store the user information and be used for validating if a user is logged in on the app or not

    // const [user, setUser] = useState(localStorage.getItem("email"));
    const [user, setUser] = useState({id:null, isAdmin:false});
    const [token, setToken] = useState(localStorage.getItem('token'));
    // Function for clearing local storage on logout
    const unSetUser =()=>{
      localStorage.removeItem("email");
      localStorage.removeItem('token');
      setUser({id:null, isAdmin:false});
      setToken(null);
    }
    
    // <UserProvider value = {{user,setUser, unSetUser}}> Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop

    // All information provided to the Provider component can be accessed later on from the context object as properties

    useEffect(()=>{
      console.log(user);
    }, [user])

    useEffect(()=>{
      
      fetch(`${process.env.REACT_APP_URL}/users/profile`,{
        headers:{
            Authorization:`Bearer ${localStorage.getItem('token')}`
      }})
      .then(res=>{
          return res.json()
      })
      .then(data=>{
          console.log(data);
          setUser({id:data._id, isAdmin:data.isAdmin});
          console.log(user);

      })
      localStorage.getItem('token')
    },[])

  return (
    <UserProvider value={{user,unSetUser, setUser, token,setToken}}>
    <Router>
      <AppNavbar/>
      <Routes>
     
        <Route  path="/" element ={<Home/>}/>

        <Route  path="/courses" element ={<Courses/>}/>

        <Route  path="/courses/:courseId" element = {<CourseView/>} />
        <Route  path="/register" element ={<Register/>}/>
        <Route  path="/login" element ={ <Login />}/>
        <Route  path="/logout" element ={ <Logout/>}/>
        <Route  path="*" element ={ <Error/>}/>
      </Routes>
    </Router>
      
    </UserProvider>
   
  );
}

export default App;
