import React from 'react';
import { useState } from 'react';
// Create a context Object
// A context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app
// The context object is a different approach to passsing information between components and allows easier access between components and allows easier access by the use of prop-drilling

// CreateContext is use to create a context in react



const UserContext = React.createContext();


export const UserProvider = UserContext.Provider;


// export function UserProvider ({children}) {
//     const [user, setUser] = useState(localStorage.getItem("email"));
    
//     // Function for clearing local storage on logout
//     const unSetUser =()=>{
//     localStorage.removeItem("email");
//     setUser(null);
//     }

//     return (
//         <UserContext.Provider value={{user,setUser,unSetUser}}>{children}</UserContext.Provider>
//     );
// }

// The "Provider" component allows other component to consume/use the context object

export default UserContext