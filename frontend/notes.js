// Syntax: npx create-react-app "project-name"

// After creating project execute the command "npm start" to run our project in our localhost

// We delete the unnecessary files from the newly created project
	// 1. App.test.js
	// 2. index.css
	// 3. logo.svg
	// 4. reportWebVitals.js

// After we deleted the unnecessary files, we encountered errors
	// errors encountered: importation of the deleted files
	// to fix we remove the syntax or codes of the imported files

//Now we have a blank state where we can start building our own ReactJS app.

// Important Note:
	// similar to NodeJS and expressJS, we can install packages in our react application to make the work easier for us
		//example : npm install react-bootstrap bootstrap

	//The "import" statement allows us to use the code/exported modules from files similar to how we use the require keyword in NodeJS

	// ReactJS components are independent, reusable pieces of code which normally contains Javascript and JSX syntax which make up a part of our application.

